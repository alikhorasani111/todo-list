import TodoForm from "./component/todo/TodoForm";
import TodoList from "./component/todo/TodoList";
import React, {useEffect, useState} from "react";
import NavbarTodo from "./component/todo/NavbarTodo";
import SelectTodo from "./component/todo/SelectTodo";

function App() {
    const [todos, setTodos] = useState([]);
    const [selected, setSelected] = useState("all");
    const [filter, setFilter] = useState([]);



    useEffect(() => {
        filterTodo(selected.value);

    }, [todos,selected]);


    const selectedHandler = (e) => {
        setSelected(e);
    };

    const filterTodo = (e) => {
        switch (e) {
            case "completed":
                setFilter(todos.filter((todo) => todo.isCompleted));
                break;
            case "uncompleted":
                setFilter(todos.filter((todo) => !todo.isCompleted));
                break;
            default:
                setFilter(todos)
        }
    };




    const addTodoHandler = (input) => {
        const newTodo = {id: Math.floor(Math.random() * 100), text: input, isCompleted: false};
        setTodos([...todos, newTodo])
    };

    const onUpdateTodo = (id, newInput) => {
        const index = todos.findIndex((todo) => todo.id === id);
        const cloneIndex = {...todos[index]};
        cloneIndex.text = newInput;

        const updatedTodos = [...todos];
        updatedTodos[index] = cloneIndex;
        setTodos(updatedTodos)
    };

    const onComplete = (id) => {
        const index = todos.findIndex((todo) => todo.id === id);
        const cloneIndex = {...todos[index]};
        cloneIndex.isCompleted = !cloneIndex.isCompleted;

        const updatedTodos = [...todos];
        updatedTodos[index] = cloneIndex;
        setTodos(updatedTodos)

    };
    const deletedHandler = (id) => {
        const deleted = todos.filter((t) => t.id !== id);
        setTodos(deleted);
    };

    const unCompleted = () => {
        return todos.filter((todo) => !todo.isCompleted).length;
    };




    return (
        <div>
            <NavbarTodo remaining={unCompleted()}/>

            <TodoForm addTodoHandler={addTodoHandler}/>
            <SelectTodo onchange={selectedHandler}  selected={selected} setSelected={setSelected}/>
            <TodoList todos={filter}
                      onUpdateTodo={onUpdateTodo}
                      onComplete={onComplete}
                      deletedHandler={deletedHandler}
            />

        </div>
    );
}

export default App;
