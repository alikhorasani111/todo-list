import Styles from '../../assets/scss/app.module.scss'

const NavbarTodo = ({remaining}) => {

    return (
        <div className={Styles.nav}>
            {remaining ?
                <>
                    {remaining} are not completed
                </>
                :
                <>
                    <h2>set yor tody todos !</h2>
                </>
            }

        </div>
    )
};


export default NavbarTodo;