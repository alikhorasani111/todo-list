import Styles from '../../assets/scss/app.module.scss';
import Select from 'react-select'

const options = [
    {value: "all", label: "all"},
    {value: "completed", label: "completed"},
    {value: "uncompleted", label: "uncompleted"},
];
const SelectTodo = ({selected, onchange}) => {

    return (
        <>
            <Select
                value={selected}
                onChange={onchange}
                className={Styles.select}
                options={options}
            />
        </>
    )

};
export default SelectTodo;