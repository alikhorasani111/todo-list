import Styles from "../../assets/scss/app.module.scss"

const Todo=({todo,editHandler,onComplete,isCompleted,onDeletedHandler})=>{
    return(
        <div className={Styles.list_item}>
            <ul>
                <li>
                    <div className={Styles.todo__style}>
                        <p className={isCompleted ? "" : `${Styles.underText}`}>{todo}</p>
                        <div className={Styles.todo__style_btn}>
                            <button onClick={editHandler} className={Styles.btn_info}>edit</button>
                            <button onClick={onComplete} className={Styles.btn_black}>Completed</button>
                            <button onClick={onDeletedHandler} className={Styles.btn_danger}>deleted</button>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    )
};
export default Todo