import Styles from '../../assets/scss/app.module.scss'
import {useState, useRef, useEffect} from "react";


const TodoForm = ({addTodoHandler, edit}) => {
    const [input, setInput] = useState(edit ? edit.text : "");
    const inputRefFocus = useRef(null);


    useEffect(() => {
        inputRefFocus.current.focus();
    }, []);

    const onChangeHandler = (e) => {
        setInput(e.target.value);
    };


    const submitHandler = (e) => {
        e.preventDefault();
        if (!input) {
            alert('enter todo');
            return;
        }
        addTodoHandler(input);
        setInput("");
    };
    return (
        <div className={Styles.container}>
            <form className={Styles.form_group} onSubmit={submitHandler}>
                <input type="text"
                       value={input}
                       onChange={onChangeHandler}
                       className={Styles.input}
                       placeholder={edit ? "edit to do..." : "add to do..."}
                       ref={inputRefFocus}
                />
                <button className={Styles.btn_danger} type="submit">{edit ? "Update Todo" : "Add Todo"}</button>
            </form>
        </div>
    )
};

export default TodoForm;