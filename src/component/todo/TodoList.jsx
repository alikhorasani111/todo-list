import Styles from "../../assets/scss/app.module.scss"
import React, {useState} from "react";
import Todo from "./Todo";
import TodoForm from "./TodoForm";

const TodoList = ({todos, onUpdateTodo, onComplete, deletedHandler}) => {
    const [edit, setEdit] = useState({id: null, text: "", isCompleted: false});

    const editTodoHandler = (newInput) => {
        onUpdateTodo(edit.id,newInput);

        setEdit({id:null,text:""})
    };


    if (todos.length === 0) {
        return <h2 className={Styles.mx_auto}>add some todos</h2>
    }

    return (
        <div>
            {edit.id ? <TodoForm addTodoHandler={editTodoHandler} edit={edit}/> :
                <div className={Styles.list}>
                    {todos.map((todo) => {
                        return (
                            <Todo
                                key={todo.id}
                                todo={todo.text}
                                isCompleted={todo.isCompleted}
                                onComplete={() => onComplete(todo.id)}
                                onDeletedHandler={() => deletedHandler(todo.id)}
                                editHandler={() => setEdit(todo)}
                            />
                        )
                    })}

                </div>}
        </div>


    );

};
export default TodoList